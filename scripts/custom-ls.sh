#!/bin/bash
if [ $# -eq 2 ] && [ $1 == "-s" ] ;then
    stat=1
else
    stat=0
fi
# Help section
if [ $# -eq 1 ] && [ $1 == "-h" ] ;then
    echo -e "\e[1mUSAGE : \e[0m"
    echo -e "./custom-ls.sh [OPTIONS] [ARGUMENTS] "
    echo -e "\e[1mOPTIONS : \e[0m"  ;
    echo -e "\e[1;35m-s \e[0m: Show list of files with its size"
    echo -e  "\e[1;35m-h \e[0m: Shows help " ;
    echo -e "\e[1mNOTE \e[0m: If you used the -s flag you have to specify the file/dir "   ;
    exit;
fi
case $# in 
    1) cd $1 ;;
    2) cd $2 ;;
esac
myls(){
    for file in $(ls -a | sort );do
    if [ -d $file ] ; then
    n=$(ls -la $file| wc -l)
        if [ $n -eq 3 ] ;then
        printf "\e[1;35m \e[0m"    
        else 
        printf "\e[1;34m \e[0m"    
        fi
        printf "$file"
      if [ $stat -eq 1 ] ; then
          printf " \e[1;36m[$(du -s -h  $file 2> /dev/null| awk '{print $1}' )]\e[0m"
    fi
    printf "\n"
    else
        ext=$(echo $file | awk -F '.' '{print$2}')
            case $ext in 
          png|jpeg|jpg) printf "\e[1;37m \e[0m" ;;
          bash|sh|ps1|zsh|fish )  printf "\e[1;36m \e[0m" ;;
          c )     printf "\e[1;33m \e[0m" ;;
          md )  printf "\e[1;33m \e[0m" ;;
          lua )  printf "\e[1;33m \e[0m" ;;
          json )  printf "\e[1;33m \e[0m" ;;
          js) printf "\e[1;33m \e[0m";; 
          java) printf "\e[1;33m \e[0m" ;;
          go) printf "\e[1;33m \e[0m" ;;
          docker) printf "\e[1;33m \e[0m" ;;
          diff) printf "\e[1;33m \e[0m" ;;
          sgl|db) printf "\e[1;33m \e[0m" ;;
          dart) printf "\e[1;33m \e[0m" ;;
          css) printf "\e[1;33m \e[0m" ;;
          cpp) printf "\e[1;33m \e[0m" ;;
          rb) printf "\e[1;33m \e[0m" ;;
          coffe) printf "\e[1;33m \e[0m" ;;
          html) printf "\e[1;33m \e[0m" ;;
          ml) printf "\e[1;33mλ \e[0m" ;;
          php) printf "\e[1;33m \e[0m" ;;
          pl) printf "\e[1;33m \e[0m" ;;
          py*) printf "\e[1;33m \e[0m" ;;
          r) printf "\e[1;33mﳒ \e[0m" ;;
          rs) printf "\e[1;33m \e[0m" ;;
          sass) printf "\e[1;33m \e[0m" ;;
          scala) printf "\e[1;33m \e[0m" ;;
          ini|rc|toml) printf "\e[1;31m \e[0m" ;;
          ts) printf "\e[1;33m \e[0m" ;;
          tsx) printf "\e[1;33m \e[0m" ;;
          vim)printf "\e[1;33m  \e[0m" ;;
          vue) printf "\e[1;33m﵂ \e[0m" ;;
          pdf) printf "\e[1;33m \e[0m" ;;
          cfg) printf "\e[1;33m \e[0m" ;;
          mp|opus|m4a) printf "\e[1;33m  \e[0m" ;;
          pro) printf "\e[1;33m \e[0m" ;;
          hs) printf "\e[1;33m \e[0m" ;;
          *) printf "\e[1;33m \e[0m" ;;  
        esac
    printf "$file"
      if [ $stat -eq 1 ] ; then
          printf " \e[1;36m[$(du -s -h  $file 2> /dev/null | awk '{print $1}' )]\e[0m"
    fi
    printf "\n"
    fi
    printf "\n"
done
}
myls | column -s '\t' | column -t
