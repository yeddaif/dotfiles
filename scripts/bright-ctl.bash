case "$1" in
    "-up")
    brightnessctl s +2%
    val=$(brightnessctl | head -n 2 | tail -n 1 | awk '{print$4}')
   dunstify -i /hdd/Icons/sun.png -h string:x-dunst-stack-tag:bright "$val" 
   ;;
    "-down")
    brightnessctl s 2%-;
    val=$(brightnessctl | head -n 2 | tail -n 1 | awk '{print$4}')
   dunstify -i /hdd/Icons/sun.png -h string:x-dunst-stack-tag:bright "$val" 
   ;;
esac
