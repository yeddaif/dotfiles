case "$1" in
"-to" )
    mpc toggle ;
    STAT=$(mpc status | head -n 2 | tail -n 1 | awk '{print$1}')
    CUR=$(mpc current)
    if [[ "$STAT" == "[paused]" ]]; then
    dunstify -i /hdd/Icons/pause-button.png -h string:x-dunst-stack-tag:mpd "$CUR is paused"
    elif  [[ "$STAT" == "[playing]" ]]; then  
    dunstify -i /hdd/Icons/play-button.png -h string:x-dunst-stack-tag:mpd "$CUR is playing"
    fi     
    ;;
"-pn" )
    mpc next;
    dunstify -i /hdd/Icons/next-button.png -h string:x-dunst-stack-tag:mpd "Playing Next  $(mpc current)"                       
    ;;
"-pp" )
    mpc prev ;   
    dunstify -i /hdd/Icons/previous.png -h string:x-dunst-stack-tag:mpd "Playing Previous $(mpc current)"                     
    ;;
"-st" )
    mpc stop ;
    dunstify -i /hdd/Icons/stop-button.png -h string:x-dunst-stack-tag:mpd "The player is stoped"
    ;;
"-sf" )
    dunstify -i /hdd/Icons/forward.png -h string:x-dunst-stack-tag:mpd "Seeking Forward 
    $(mpc seek +1% | head -n 2 | tail -n 1 | awk '{print$3}')"  
    ;;
"-sb" )
    dunstify -i /hdd/Icons/backward.png  -h string:x-dunst-stack-tag:mpd "Seeking Backward 
    $(mpc seek -1% | head -n 2 | tail -n 1 | awk '{print$3}')"  
    ;;
*)
    echo "Bad argument"
    ;;
esac
