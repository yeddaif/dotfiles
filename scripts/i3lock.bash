i3lock -n -i /hdd/Pictures/Wallpapers/arch-linux.png 

# Turn the screen off after a delay.
sleep 150; pgrep i3lock && xset dpms force off

# Hibernate after 15min
sleep 300; pgrep i3lock && systemctl hibernate
