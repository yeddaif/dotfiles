Model=LBP3150
# Check for cups status
srv_manage(){
if [ "$(systemctl is-enabled $1)" == "disabled" ]; then
    echo "Enabling $1"
    systemctl enable  $1
else
    echo "$1 already enabled !"
fi
sleep 1;
if [ "$(systemctl is-active $1)" == "inactive" ]; then
    echo "Starting $1"
    systemctl start $1
else
    echo "$1 already started !"
fi
}
srv_manage cups ;
# Adding Printer Device
if [ -e "/etc/cups/ppd/${Model}.ppd" ];then
    echo "${Model} Printer already added !"
else 
    echo "Adding ${Model} Priner ..."
    /usr/bin/lpadmin -p $Model -m CNCUPS${Model}CAPTK.ppd -v ccp://localhost:59687 -E  
    /usr/bin/ccpdadmin -p $Model -o /dev/usb/lp0
fi
srv_manage ccpd ;
