import os
import subprocess

from libqtile import hook

from typing import List  # noqa: F401

from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

mod = "mod4"
terminal = guess_terminal()

keys = [
    # Switch between windows

    Key([mod], "Left", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "Right", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "Down", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "Up", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "a", lazy.layout.next(),
        desc="Move window focus to other window"),
    # Move windows

    Key([mod, "shift"], "Left", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "Right", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows

    Key([mod, "control"], "Left", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "Right", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "Down", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "Up", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    Key([mod, "shift"],
        "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack", ),
    # Works only on monad mode
    Key([mod], "n",
        lazy.layout.normalize(), desc='normalize window size ratios'),
    Key([mod], "m", lazy.layout.maximize(),
        desc='toggle window between minimum and maximum sizes'),
    Key([mod, "shift"], "f",
        lazy.window.toggle_floating(), desc='toggle floating'),
    Key([mod], "f",
        lazy.window.toggle_fullscreen(), desc='toggle fullscreen'),
    # Stack controls
    Key([mod], "z",
        lazy.layout.rotate(),
        lazy.layout.flip(),
        desc='Switch which side main pane occupies (XmonadTall)'
        ),
    Key([mod], "w",
        lazy.layout.next(),
        desc='Switch window focus to other pane(s) of stack'
        ),
    Key([mod], "e",
        lazy.layout.toggle_split(),
        desc='Toggle between split and unsplit sides of stack'
        ),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "c", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd("run :"),
        desc="Spawn a command using a prompt widget"),
    # Applications
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    Key([mod], "b", lazy.spawn('brave'), desc="Browser"),
    Key([mod], "d", lazy.spawn('pcmanfm'), desc="Files Manager"),
    Key([mod], "space",
        lazy.spawn('rofi -show drun -theme .config/rofi/dmenu-like.rasi'),
        desc="Launcher"),
    # Media Control
    Key([], "XF86MonBrightnessUp", lazy.spawn("brightnessctl s +5%")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl s 5%- ")),

]

# Autostart Hook


@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.run([home])


groups = []

# FOR AZERTY KEYBOARDS
group_names = ["ampersand", "eacute", "quotedbl", "apostrophe", "parenleft"]

group_labels = ["", "", "", "", ""]

group_layouts = ["columns", "columns", "columns", "columns", "columns"]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))

for i in groups:
    keys.extend(
        [
            # Switch to group
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
                desc="Switch to group {}".format(i.name),
            ),
            # Switch to & move focused window to group
            Key(
                [mod, "shift"],
                i.name,
                lazy.window.togroup(i.name, switch_group=True),
                desc="Switch to & move focused window to group {}".format(
                    i.name),
            ),
        ]
    )

layouts = [
    layout.Columns(
        border_focus_stack=["#d75f5f", "#8f3d3d"],
        border_width=2,
        border_focus="#fe8019",
        border_normal="#7c6f64",
        font="JetBrainsMono",
        fontsize=10,
        margin=3
    ),
    layout.Max(margin=3),
    # Try more layouts by unleashing below layouts.
    layout.Stack(
        border_focus="#fe8019",
        border_width=2,
        border_normal="#7c6f64",
        margin=3, num_stacks=2),
    layout.MonadTall(
        font="JetBrainsMono",
        border_width=2,
        border_focus="#fe8019",
        border_normal="#7c6f64",
        fontsize=10,
        margin=3
    ),
    layout.TreeTab(
        font="JetBrainsMono",
        fontsize=13,
        sections=[" "],
        section_fontsize=14,
        border_width=2,
        border_focus="#fe8019",
        border_normal="#7c6f64",
        bg_color="#272727",
        active_bg="#fb4934",
        active_fg="#fb4934",
        inactive_bg="#83a598",
        inactive_fg="#83a598",
        padding_left=0,
        padding_x=0,
        padding_y=5,
        section_top=0,
        section_bottom=20,
        level_shift=8,
        vspace=3,
        panel_width=26,
        margin=3

    ),
]

widget_defaults = dict(
    font="JetBrainsMono",
    background="#272727",
    fontsize=12,
    padding=3
)


def separator():
    return widget.Sep(
        linewidth=0,
        padding=10
    )


def right_corner():
    return widget.TextBox(
        text='\ue0b4',
        foreground="#504945",
        padding=0,
        fontsize=26)


def left_corner():
    return widget.TextBox(
        text='\ue0b6',
        foreground="#504945",
        padding=0,
        fontsize=26)


extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [

                left_corner(),
                widget.GroupBox(
                    background="#504945",
                    block_highlight_text_color="#fabd2f",
                    highlight_color=["#7c6f64", "#7c6f64"],
                    active="#83a598",
                    disable_drag=True,
                    highlight_method='line',
                    inactive="#dfdfdf",
                ),
                widget.TextBox(
                    text='\ue0b4',
                    foreground="#504945",
                    padding=0,
                    fontsize=37),
                separator(),
                widget.Prompt(),
                left_corner(),
                widget.WindowName(background="#504945",
                                  max_chars=10, padding=9),
                right_corner(),
                separator(),
                widget.Spacer(length=760),
                left_corner(),
                widget.CurrentLayout(
                    background="#504945",
                    fontsize=12,
                ),
                right_corner(),
                separator(),
                widget.Chord(
                    chords_colors={
                        "launch": ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                left_corner(),
                widget.Systray(
                    background="#504945"
                ),
                right_corner(),
                separator(),
            ],
            24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),
        Match(wm_class="Lxappearance"),
        Match(wm_class="Pavucontrol"),
        Match(wm_class="Gcolor3"),
        Match(wm_class='download'),
        Match(wm_class='Lxpolkit'),

    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True
auto_minimize = True
wmname = "LG3D"
