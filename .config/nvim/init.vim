"----------------------------------------------------------------
"       ____       _   _   _
"      / ___|  ___| |_| |_(_)_ __   __ _ ___
"      \___ \ / _ \ __| __| | '_ \ / _` / __|
"      ___) |  __/ |_| |_| | | | | (_| \__ \
"      |____/ \___|\__|\__|_|_| |_|\__, |___/
"                                  |___/
"----------------------------------------------------------------
"set spell spelllang=en
syntax on
set encoding=UTF-8
set number
set relativenumber
set ignorecase
set smartcase
set smarttab
set nocompatible
set incsearch
set visualbell
set expandtab
set tabstop=4
set ruler
set smartindent
set shiftwidth=4
set hlsearch
set virtualedit=all
set backspace=indent,eol,start
set autoindent
set mouse=a
set laststatus=2
set showtabline=2
set noshowmode
set completeopt=menuone,noselect
filetype plugin on
set termguicolors
set t_Co=256
set guicursor=
set clipboard=unnamedplus
"-------------------------- Plugins ---------------
call plug#begin('~/.local/nvim/plugins')
    Plug 'neovim/nvim-lspconfig'
    Plug 'hrsh7th/nvim-compe'
    Plug 'akinsho/bufferline.nvim'
    Plug 'hoob3rt/lualine.nvim'
    Plug 'kyazdani42/nvim-web-devicons'
    Plug 'jiangmiao/auto-pairs'
    Plug 'lilydjwg/colorizer'
    Plug 'yeddaif/neovim-purple'
    Plug 'tpope/vim-fugitive'
    Plug 'mhinz/vim-signify'
call plug#end()
"-------------------------- Theming ---------------
set background=dark
set cursorcolumn
set cursorline
let g:nobackground = 1
colorscheme neovim_purple

"""" Keys

"------------- Easy CAPS --------------------------
inoremap <c-u> <ESC>viwUi
nnoremap <c-u> viwU<Esc>

"------------- Buffers Action ---------------------
nnoremap <TAB> :bnext<CR>
nnoremap <S-TAB> :bprevious<CR>
nnoremap <C-d> :bdelete<CR>

"------------- Save and Quit ----------------------
nnoremap <C-s> :w<CR>
nnoremap <C-q> :q<CR>
"------------- Better tabbing ---------------------
vnoremap < <gv
vnoremap > >gv
"------------- compe completion -------------------
inoremap <silent><expr> <CR> compe#confirm('<CR>')

"--------------------------------------------------
"               Lua Files
"--------------------------------------------------
luafile $HOME/.config/nvim/lua/compe-conf.lua
luafile $HOME/.config/nvim/lua/buffer.lua
luafile $HOME/.config/nvim/lua/statusline.lua
luafile $HOME/.config/nvim/lua/icons.lua
luafile $HOME/.config/nvim/lua/signify.lua
